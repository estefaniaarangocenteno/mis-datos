const PackageJson = require('../package.json');

const {
    TIMEZONE,
} = process.env;

module.exports = {
    APP_NAME: PackageJson.name,
    APP_VERSION: PackageJson.version,
    TIMEZONE: TIMEZONE || 'America/Bogota',
};