const router = require('express').Router();

const HealthCheckController = require('../controllers/HealthCheckController');
const UserDataController = require('../Controllers/UserDataController');

router.get('/health-check', HealthCheckController.check);

module.exports = router;

