const Settings = require('../configs/Settings');

const HealthCheckController = module.exports;

HealthCheckController.check = (req, res) => res.send({
    appName: Settings.APP_NAME,
    appVersion: Settings.APP_VERSION,
});