const CreateTransactionService = require('../services/CreateTransactionService');
const ExportTransactionHistoryService = require('../services/ExportTransactionHistoryService');
const InactivateTransactionService = require('../services/InactivateTransactionService');
const TotalUserPointsService = require('../services/TotalUserPointsService');
const UserLoginService = require('../services/UserLoginService');
const UserRegisterService = require('../services/UserRegisterService');
const UserTransactionHistoryService = require('../services/UserTransactionHistoryService');

const UserDataController = module.exports;

UserDataController.registerUser = async ({ body, headers }, res) => {

}

UserDataController.loginUser = async ({ body, headers }, res) => {

}

UserDataController.createTransaction = async ({ body, headers }, res) => {

}

UserDataController.getTransactionHistory = async ({ headers }, res) => {

}

UserDataController.inactivateTransaction = async ({ headers }, res) => {

}

UserDataController.getTotalUserPoints = async ({ headers }, res) => {

}

UserDataController.exportTransactionHistory = async ({ body, headers }, res) => {

}

